# git 멀티 모듈 프로젝트 gradle 구성

### 프로젝트 구조
```
api-server/ -|
             |
	         - api-coomon/ -|
			                | .git
		     |
		     - api-local/ -|
			               | .git
                           | Dockerfile
			 |
			 | .git/ -|
			          | config
             |
			 | .gitmodules
			 | build.gradle
			 | settings.gradle
		 
```

### .git/config
```
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
        ignorecase = true
        precomposeunicode = true
[submodule]
        active = .
[remote "origin"]
        url = https://connect2sys@github.com/[username]/api-server
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master
[submodule "api-commons"]
        url = https://connect2sys@github.com/[username]/api-common.git
[submodule "api-local"]
        url = https://connect2sys@github.com/[username]/api-local.git
[user]
        name = connect2sys
        email =

```


### .gitmodules
```
[submodule "api-commons"]
	path = api-common
	url = https://github.com/[username]/api-common.git

[submodule "api-local"]
	path = api-local
	url = https://github.com/[username]/api-local.git
```

### build.gradle
```
...
project(':api-local') {
    dependencies {
        compile project(':api-common')
    }
}
...
```

### settings.gradle
```
rootProject.name = 'api-server'
include 'api-common','api-local'
```

### api-common/.git
```
gitdir: ../.git/modules/api-common
```

### api-local/.git
```
gitdir: ../.git/modules/api-local
```

### Dockerfile
```
FROM openjdk:11
ENV APP_HOME=/usr/app
WORKDIR $APP_HOME
COPY ./build/libs/* ./api-local-1.0.0.jar
EXPOSE 8089
CMD ["java","-jar","api-local-1.0.0.jar"]
```

### 서브모듈까지 pull
```
$ git pull origin master --recurse-submodules
```